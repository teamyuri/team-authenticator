import SimpleHTTPServer
import SocketServer
import requests
import tinydb

CLIENT_ID = "60240511031.211802389235"
CLIENT_SECRET = "0bbe12f038d9573842167d44f89f5496"
REDIRECT_URI = "http://localhost:8000/auth"

DB_FILE = 'teams.json'
PORT = 8000
keep_serving = True

class MyAuthHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    def do_GET(self):
        
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        reply = "Default"

        path, query = break_path(self.path)
        print("Params path: {}, query: {}".format(path, query))
        # this request server the page with the installation (Slack Connect) button
        if path.endswith('/index'):
            reply = read_file('index.html')

        # this request will authenticate the installation token we got from slack
        elif path.endswith('/auth'):            
            reply = authenticate_code(query['code'])

        # turns the server off
        elif path.endswith('/stop'):
            reply = stop_server()

        else:
            reply = "Your page wasn't found"

        self.wfile.write(reply)
        return


# ------------- server actions
# serve static file
def read_file(filename):
    contents = open(filename, 'r').read()
    return contents

# stop server
def stop_server():
    global keep_serving
    keep_serving = False
    print("stopping server")
    return '<div style="color:red">Stopping</div>'

# authenticate slack code -- this is the most important function bot-wise
def authenticate_code(code):
    exchange_url = "https://slack.com/api/oauth.access?client_id={}&client_secret={}&code={}&redirect_uri={}".format(CLIENT_ID, CLIENT_SECRET, code,REDIRECT_URI)
    reply = requests.get(exchange_url)
    reply_json = reply.json()
    add_team_to_db(reply_json)
    return "Your code is: {}<br>Response JSON: {}".format(code, reply_json)  # We need to implement this in order for authentication to work!

def add_team_to_db(team_json):
    db = tinydb.TinyDB(DB_FILE)
    Team = tinydb.Query()
    db_obj = {"id":team_json["team_id"], "name": team_json["team_name"], "bot_access_token": team_json["bot"]["bot_access_token"]}
    if db.search(Team.id == team_json["team_id"]):
        print("Team {} ({}) exists - updating".format(team_json["team_name"], team_json["team_id"]))
        db.update(db_obj, Team.id == team_json["team_id"])
    else:
        print("Team {} ({}) does NOT exists - inserting".format(team_json["team_name"], team_json["team_id"]))
        db.insert(db_obj)


# start server
def start_serving():
    httpd = SocketServer.TCPServer(("", PORT), MyAuthHandler)
    print "serving at port", PORT
    while keep_serving:
        httpd.handle_request()

# ------------- end of server actions


# ------------- helper functions
def break_path(path):
    broken = path.split("?")
    only_path = broken[0]
    query_obj = {} if len(broken) == 1 else build_query_obj(broken[1])
    return only_path, query_obj

def build_query_obj(query_string):
    broken = query_string.split("&")
    query_obj = {}
    for i in broken:
        kv = i.split("=")
        query_obj[kv[0]] = kv[1]

    return query_obj


start_serving()
